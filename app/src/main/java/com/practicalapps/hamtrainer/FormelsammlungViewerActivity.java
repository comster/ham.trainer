/*  vim: set sw=4 tabstop=4 fileencoding=UTF-8:
 *
 *  Copyright 2014 Matthias Wimmer
 *            2015 Dominik Meyer
 * 			  2020 William Westcott
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.practicalapps.hamtrainer;

import android.app.ActionBar;
import android.content.SharedPreferences;
import android.app.Activity;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatDelegate;

import com.github.barteksc.pdfviewer.PDFView;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.github.barteksc.pdfviewer.listener.OnPageChangeListener;

import static java.lang.Math.max;
import static java.lang.Math.min;

public class FormelsammlungViewerActivity extends Activity implements OnLoadCompleteListener, OnPageChangeListener {
    public String FORMELSAMMLUNG_FILE = "formulas.pdf";

    private SharedPreferences mPrefs;

    PDFView pdfView;

    int nbPages = 0;
    int pageToShow = 0;

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected void onPause() {
        super.onPause();
        if(pdfView != null) {
            SharedPreferences.Editor ed = mPrefs.edit();
            ed.putInt("last_page_shown", pdfView.getCurrentPage());
            ed.apply();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set activity theme and formula sheet to use based on dark theme preference
        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
            FORMELSAMMLUNG_FILE = "formulasdk.pdf";
        } else {
            setTheme(R.style.LightTheme);
            FORMELSAMMLUNG_FILE = "formulas.pdf";
        }

        mPrefs = getSharedPreferences("formelsammlung_viewer_shared_preferences", MODE_PRIVATE);
        setContentView(R.layout.activity_formelsammlung_viewer);

        ActionBar actionBar = getActionBar();
        if (actionBar != null) {
            // Show the Up button in the action bar.
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Bundle bund = getIntent().getExtras();
        if(bund != null) {
            this.pageToShow = bund.getInt(getClass().getName() + ".lichtblickPage");
        } else {
            this.pageToShow = 0;
        }

        if (this.pageToShow <= 0) {
            this.pageToShow = mPrefs.getInt("last_page_shown", 0);
        }

        pdfView = findViewById(R.id.formelsammlung_pdfview);
        pdfView.fromAsset(FORMELSAMMLUNG_FILE)
                .defaultPage(this.pageToShow)
                .enableSwipe(true)
                .enableDoubletap(true)
                .onLoad(this)
                .onPageChange(this)
                .load();
    }

    public void loadComplete(int nbPages) {
        this.nbPages = nbPages;
        this.pageToShow = max(min(nbPages, this.pageToShow), 0);
    }

    public void onPageChanged(int page, int pageCount) {

    }
}
