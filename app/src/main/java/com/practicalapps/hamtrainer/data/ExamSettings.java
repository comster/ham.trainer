/*  vim: set sw=4 tabstop=4 fileencoding=UTF-8:
 *
 *  Copyright 2014 Matthias Wimmer
 *            2015 Dominik Meyer
 * 			  2020 William Westcott
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.practicalapps.hamtrainer.data;

import java.io.Serializable;

public class ExamSettings implements Serializable {
    private int id;
    private int topicId;
    private int nQuestions;
    private int nRequired;
    private long nSecondsAvailable;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getnQuestions() {
        return nQuestions;
    }

    public void setnQuestions(int nQuestions) {
        this.nQuestions = nQuestions;
    }

    public int getnRequired() {
        return nRequired;
    }

    public void setnRequired(int nRequired) {
        this.nRequired = nRequired;
    }

    public long getnSecondsAvailable() {
        return nSecondsAvailable;
    }

    public void setnSecondsAvailable(long nSecondsAvailable) {
        this.nSecondsAvailable = nSecondsAvailable;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }
}
