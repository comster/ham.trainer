/*  vim: set sw=4 tabstop=4 fileencoding=UTF-8:
 *
 *  Copyright 2014 Matthias Wimmer
 *            2015 Dominik Meyer
 * 			  2020 William Westcott
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.practicalapps.hamtrainer.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class QuestionState implements Serializable, Parcelable {
    private static final String TAG = QuestionState.class.getName();

    private QuestionState(Parcel in) {
        questionId = in.readInt();
        order = new ArrayList<>();
        in.readList(order, QuestionState.class.getClassLoader());
        answer = in.readInt();
        listeners = new ArrayList<>();
    }

    public static final Creator<QuestionState> CREATOR = new Creator<QuestionState>() {
        @Override
        public QuestionState createFromParcel(Parcel in) {
            return new QuestionState(in);
        }

        @Override
        public QuestionState[] newArray(int size) {
            return new QuestionState[size];
        }
    };

    @Override
    public int describeContents() {
        return hashCode();
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(questionId);
        parcel.writeList(order);
        parcel.writeInt(answer);
    }

    private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        listeners = new ArrayList<>();
    }

    public int getAnswer() {
        return answer;
    }

    public interface QuestionStateListener {
        void onAnswerSelected(int answer);
    }

    private transient Question question;
    private int questionId;
    private List<Integer> order;
    private int answer = -1;
    private transient List<QuestionStateListener> listeners;

    private static final Random rand = new Random();

    public QuestionState(Question q) {
        this(q.getId());
        this.question = q;
    }

    public QuestionState(int questionId) {
        this.questionId = questionId;
        listeners = new ArrayList<>();

        this.order = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            order.add(rand.nextInt(order.size() + 1), i);
        }

        Log.d(TAG, "generated new order: " + order);
    }

    public int getQuestionId() {
        return questionId;
    }

    public String helptext;

    public Question getQuestion(Repository repository) {
        if (question == null) {
            question = repository.getQuestion(questionId);
        }
        this.helptext = question.getHelp();
        return question;
    }


    public List<Integer> getOrder() {
        return order;
    }

    public boolean hasAnswer() {
        return answer >= 0;
    }

    public boolean isCorrect() {
        return getCorrectAnswer() == answer;
    }

    public void setAnswer(int answer) {
        Log.d(TAG, "selected answer: " + answer);
        this.answer = answer;
        for (QuestionStateListener l : listeners) l.onAnswerSelected(answer);
    }

    public int getCorrectAnswer() {
        return order.get(0);
    }

    public void addQuestionStateListener(QuestionStateListener l) {
        listeners.add(l);
    }

//  public void removeQuestionStateListener(QuestionStateListener l) { listeners.remove(l); }
}
