/*  vim: set sw=4 tabstop=4 fileencoding=UTF-8:
 *
 *  Copyright 2014 Matthias Wimmer
 *            2015 Dominik Meyer
 * 			  2020 William Westcott
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.practicalapps.hamtrainer.exam;

import android.app.Activity;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
//Replaces import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.practicalapps.hamtrainer.R;
import com.practicalapps.hamtrainer.data.QuestionState;
import com.practicalapps.hamtrainer.data.Repository;
import com.practicalapps.hamtrainer.views.QuestionView;

public class ExamReportActivity extends Activity {
    private Repository repository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES) {
            setTheme(R.style.DarkTheme);
        } else {
            setTheme(R.style.LightTheme);
        }

        repository = Repository.getInstance();

        setContentView(R.layout.exam_report);

        final QuestionResults results = (QuestionResults) getIntent().getSerializableExtra(getClass().getName() + ".result");
        ExpandableListView resultsListView = findViewById(R.id.resultList);
        TextView resultsTopText = findViewById(R.id.examResultTopText);
        TextView resultsRecommendation = findViewById(R.id.examResultRecommendation);

        int nCorrect = 0;
        for(QuestionState r : results.getResults()) {
            if(r.isCorrect()) {
                nCorrect++;
            }
        }
        int nQuestions = results.getExamSettings().getnQuestions();
        int nRequired = results.getExamSettings().getnRequired();
        boolean passed = nCorrect >= nRequired;

        resultsTopText.setText(String.format(getString(R.string.exam_report_result_text), nCorrect, nQuestions));
        if (passed) {
            resultsTopText.setTextColor(Color.GREEN);
            resultsRecommendation.setText(getString(R.string.exam_result_recommendation_passed));
        } else {
            resultsTopText.setTextColor(Color.RED);
            resultsRecommendation.setText(getString(R.string.exam_result_recommendation_failed));
        }

        resultsListView.setAdapter(new ExpandableListAdapter() {
            @Override
            public boolean areAllItemsEnabled() {
                return true;
            }

            @Override
            public void registerDataSetObserver(DataSetObserver observer) {

            }

            @Override
            public void unregisterDataSetObserver(DataSetObserver observer) {

            }

            @Override
            public int getGroupCount() {
                return results.getResults().size();
            }

            @Override
            public int getChildrenCount(int i) {
                return 1;
            }

            @Override
            public Object getGroup(int i) {
                return null;
            }

            @Override
            public Object getChild(int i, int i1) {
                return null;
            }

            @Override
            public long getGroupId(int i) {
                return 0;
            }

            @Override
            public long getChildId(int i, int i1) {
                return 0;
            }


            @Override
            public boolean hasStableIds() {
                return false;
            }

            @Override
            public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
                View v;
                if (view == null) {
                    v = ExamReportActivity.this.getLayoutInflater().inflate(R.layout.exam_report_result_item, viewGroup, false);
                } else {
                    v = view;
                }
                TextView reference = v.findViewById(R.id.questionReference);
                TextView result = v.findViewById(R.id.questionResult);
                QuestionState qs = results.getResults().get(i);
                reference.setText(qs.getQuestion(repository).getReference());
                if (qs.hasAnswer()) {
                    result.setText(getResources().getString(qs.isCorrect() ? R.string.correct : R.string.wrong));
                    result.setTextColor(qs.isCorrect() ? Color.GREEN : Color.RED);
                } else {
                    result.setText(getResources().getString(R.string.not_answered));
                    //Text color was being changed when set, this sets it to neutral gray
                    result.setTextColor(Color.parseColor("#777777"));
                }
                return v;
            }

            @Override
            public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
                QuestionView v;
                if (view == null) {
                    v = new QuestionView(ExamReportActivity.this);
                } else {
                    v = (QuestionView) view;
                }
                v.setQuestionState(results.getResults().get(i));
                v.setRadioGroupEnabled(false);
                v.showCorrectAnswer();
                return v;
            }

            @Override
            public boolean isChildSelectable(int i, int i1) {
                return true;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public void onGroupExpanded(int i) {

            }

            @Override
            public void onGroupCollapsed(int i) {

            }

            @Override
            public long getCombinedChildId(long l, long l1) {
                return 0;
            }

            @Override
            public long getCombinedGroupId(long l) {
                return 0;
            }
        });

    }
}
