/*  vim: set sw=4 tabstop=4 fileencoding=UTF-8:
 *
 *  Copyright 2014 Matthias Wimmer
 *            2015 Dominik Meyer
 * 			  2020 William Westcott
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.practicalapps.hamtrainer.exam;

import java.io.Serializable;
import java.util.List;

import com.practicalapps.hamtrainer.data.ExamSettings;
import com.practicalapps.hamtrainer.data.QuestionState;

public class QuestionResults implements Serializable {
    private List<QuestionState> results;
    private ExamSettings examSettings;

    public QuestionResults(final List<QuestionState> rl, final ExamSettings e) {
        this.results = rl;
        this.examSettings = e;
    }

    public List<QuestionState> getResults() {
        return results;
    }

    public ExamSettings getExamSettings() {
        return examSettings;
    }
}