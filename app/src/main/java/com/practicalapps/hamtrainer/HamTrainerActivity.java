/*  vim: set sw=4 tabstop=4 fileencoding=UTF-8:
 *
 *  Copyright 2014 Matthias Wimmer
 *            2015 Dominik Meyer
 * 			  2020 William Westcott
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.practicalapps.hamtrainer;
import com.practicalapps.hamtrainer.data.Repository;
import com.practicalapps.hamtrainer.exam.QuestionListActivity;

import java.text.DateFormat;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.SimpleCursorAdapter.ViewBinder;
import android.widget.TabHost;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatDelegate;


public class HamTrainerActivity extends Activity {
	private Repository repository;
    SimpleCursorAdapter adapter;
    private boolean dm_state;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        manageTheme(0);

        setContentView(R.layout.main);
        TabHost th = findViewById(R.id.tabhost);
        th.setup();

        // TODO strings
        TabHost.TabSpec ts1 = th.newTabSpec("learn");
        ts1.setIndicator(getString(R.string.learning));
        ts1.setContent(R.id.tab1);
        th.addTab(ts1);

        TabHost.TabSpec ts2 = th.newTabSpec("simulate");
        ts2.setIndicator(getString(R.string.exam_simulation));
        ts2.setContent(R.id.tab2);
        th.addTab(ts2);

        //On some devices the tab text color stays black after dark theme is applied
        //This block changes text color to white if dark theme is on and it's not white already
        if(AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES){
            for(int i = 0;i < th.getTabWidget().getChildCount(); i++)
            {
                TextView textView = th.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
                Integer color = textView.getCurrentTextColor();
                if(!color.equals(R.color.white)){
                    textView.setTextColor(getResources().getColor(R.color.white));
                }
            }
        }

        repository = Repository.getInstance();
    }

    public void manageTheme(Integer cw) {
        //Retrieve preference settings (Current string is default table)
        SharedPreferences dmpref = getSharedPreferences("com.practicalapps.hamtrainer_preferences", MODE_PRIVATE);
        //Select dark theme preference and assign value to dm_on, default value is off/false/light theme
        boolean dm_on = dmpref.getBoolean("pref_dark_theme", false);
        //Set defaultnightmode based on dark_theme preference
        if (dm_on) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
        else {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }
        int dmode = AppCompatDelegate.getDefaultNightMode();
        if(cw == 0) { //When called from onCreate
            //Set theme based on defaultnightmode value and record state
            if(dmode == AppCompatDelegate.MODE_NIGHT_YES) {
                setTheme(R.style.DarkTheme);
                dm_state = true;
            } else {
                setTheme(R.style.LightTheme);
                dm_state = false;
            }
        }
        else{ //When called from onResume
            //If there is a mismatch between current and saved state, restart activity
            if(dm_state && dmode == AppCompatDelegate.MODE_NIGHT_NO || !dm_state && dmode == AppCompatDelegate.MODE_NIGHT_YES) {
                finish();
                startActivity(new Intent(HamTrainerActivity.this, HamTrainerActivity.this.getClass()));
            }
        }
    }
    
    @Override
    public void onResume() {
    	super.onResume();

        manageTheme(1);
    	
    	final ListView topicList = findViewById(R.id.topic);
        
        this.adapter = new SimpleCursorAdapter(this,
                R.layout.topic_list_item,
                null,
                new String[]{"name", "status", "next_question"},
                new int[]{R.id.topicListItem, R.id.topicStatusView, R.id.nextQuestionTime});

        adapter.setViewBinder(new ViewBinder() {
            @SuppressLint("SetTextI18n")
            public boolean setViewValue(View view, Cursor cursor,
                                        int columnIndex) {

                if (columnIndex == 4) {
                    final TextView textView = (TextView) view;
                    if (!cursor.isNull(4)) {
                        final long nextQuestion = cursor.getLong(4);
                        final long now = new Date().getTime();
                        if (nextQuestion > now) {

                            if (nextQuestion - now < 64800000L) {
                                textView.setText(getString(R.string.nextLabel) + " " + DateFormat.getTimeInstance().format(new Date(nextQuestion)));
                            } else {
                                textView.setText(getString(R.string.nextLabel) + " " + DateFormat.getDateTimeInstance().format(new Date(nextQuestion)));
                            }
                            return true;
                        }
                    }
                    textView.setText("");
                    return true;
                }

                return false;
            }
        });
        topicList.setAdapter(adapter);
        repository.setTopicsInSimpleCursorAdapter(adapter);
        
        topicList.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			//@Override
			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {

				final Intent intent = new Intent(HamTrainerActivity.this, AdvancedQuestionAsker.class);
				intent.putExtra(AdvancedQuestionAsker.class.getName() + ".topic", id);
				startActivity(intent);
			}
		});


        final ListView examTopicList = findViewById(R.id.examTopic);

        SimpleCursorAdapter examTopicAdapter = new SimpleCursorAdapter(
                this,
                R.layout.exam_list_item,
                null,
                new String[]{"name"},
                new int[]{R.id.topicListItem},
                0
        );

        examTopicList.setAdapter(examTopicAdapter);
        repository.setExamTopicsInSimpleCursorAdapter(examTopicAdapter);

        examTopicList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Intent intent = new Intent(HamTrainerActivity.this, QuestionListActivity.class);
                intent.putExtra(QuestionListActivity.class.getName() + ".topic", id);
                startActivity(intent);
            }
        });
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    	
    	final ListView topicList = findViewById(R.id.topic);
    	
    	final SimpleCursorAdapter adapter = (SimpleCursorAdapter) topicList.getAdapter();
    	final Cursor previousCursor = adapter.getCursor();
    	adapter.changeCursor(null);
    	previousCursor.close();
    	topicList.setAdapter(null);
    }

	/**
	 * Populate options menu.
	 *
	 * @param menu the menu to populate
	 * @return always true
	 */
	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.mainmenu, menu);
		return true;
	}

	/**
	 * Handle option menu selections.
	 *
	 * @param item the Item the user selected
	 */
	@Override
    public boolean onOptionsItemSelected(final MenuItem item) {
		//Options for main menu settings, switch-case for item selection, see res/menu/mainmenu.xml
		switch (item.getItemId()) {
            case R.id.mainSettings:
                final Intent intentSettings = new Intent(this, SettingsActivity.class);
                startActivity(intentSettings);
                return true;
			case R.id.mainSearch:
                final Intent intent = new Intent(HamTrainerActivity.this, QuestionSearch.class);
                startActivity(intent);
                return true;
            case R.id.mainFormelsammlung:
                final Intent intentFormelsammlung = new Intent(this, FormelsammlungViewerActivity.class);
                final int formelsammlungPage = 0;
                intentFormelsammlung.putExtra(FormelsammlungViewerActivity.class.getName() + ".formelsammlungPage", formelsammlungPage);
                startActivity(intentFormelsammlung);
                return true;
            case R.id.reportError:
                final Intent reportintent = new Intent(Intent.ACTION_VIEW);
                reportintent.setData(android.net.Uri.parse("https://gitlab.com/Practical.Apps/ham.trainer/-/issues/new"));
                startActivity(reportintent);
                return true;
            case R.id.About:
                final Intent intentAbout = new Intent(this, AboutPage.class);
                startActivity(intentAbout);
                return true;
		    default:
			    return super.onOptionsItemSelected(item);
		}
	}
}
