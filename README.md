HAMtrainer
===========
HAMtrainer is the number 1 free, libre, and open source American ham radio test prep android application available.
You can use this app to prepare for any of the US amateur radio license exams.

HAX!

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
     alt="Get it on F-Droid"
     height="75">](https://f-droid.org/packages/com.practicalapps.hamtrainer/)

[![forthebadge](https://forthebadge.com/images/badges/made-with-java.svg)](https://forthebadge.com)[![forthebadge](https://forthebadge.com/images/badges/built-for-android.svg)](https://forthebadge.com)

**Features**

-Train for any of the three license exams: Technician, General, Extra

-The newest question pools are available to study. Valid through:

Technician Class: 7/1/2018 - 6/30/2022, General Class: 7/1/2019 - 6/30/2023, Extra Class: 7/1/2020 - 6/30/2024

(Option to choose to to include over 100 "outdated" questions that are no longer in the official pools)

-General study mode to learn and exam simulation mode for practice

-Track your progress and focus on training questions that still challenge you

-Optional dark theme available on all features to reduce eye strain in low light

-German translations (Menu items and descriptions, not questions or answers)

**Un-features**

-No extra permission requests

-No internet connection required

-No tracking, usage statistics, analytics, background telemetry connections of any kind

-No accounts/profiles, sign-up/sign-in, etc.

-No nagging to rate the app, trial periods, paid upgrades/add-ons, etc.

-Backwards compatible to Android 4.0/API 14, limited by device performance rather than android version. 
v.1.0.7 Confirmed working on Android 4.2.2/API 17, if it works on your older device, let me know.

**Source/License Info**

HAMtrainer is a fork of Funktrainer by D. Meyer (2015), the premier test preparation android app for the German Amateurfunklizenz.
I have preserved the German translations in the app on the off chance users with German set as their android locale will want to use them.
This application could be made because Funktrainer source code was licensed for use under the Apache License version 2.
The modified source code for this application is available to you under the same license, which can be found in the main project folder.

Copyright 2020 William Westcott

NOTICE FOR COMPLIANCE WITH APACHE LICENSE V2.0 (SECTION 4)

HAMtrainer source code is available at https://gitlab.com/Practical.Apps/ham.trainer
HAMtrainer is forked from Dominik Meyer's 'Funktrainer' app for the German funk (wireless) license exams.
Source code for Funktrainer can be found here: https://github.com/meyerd/funktrainer
Funktriner itself is based on source code from Matthias Wimmer's 'Sailtrainer' app for German watercraft license exams.
Source code for Sailtrainer can be found here: https://github.com/mawis/sailtrainer

Comment blocks have been added/editted in files which were altered in making the new program.
If the comment block contains my name, I edited the file in some way. In java files these precede the code while in xml files the comment blocks follow after.
Files without comment blocks for the copyright notice never carried one and were unaltered.

**Developer Info**

Any messages relating to programming (compliments, complaints, questions, solicitations, requests, threats etc.) are welcome at practicalapps@tutanota.com - Feel free to contribute code to this project here on gitlab. Even if you cannot code, providing help text for questions would be very much appreciated.

To those looking for how to convert exam text to the SQL schema used in the app, I have uploaded the script I used as [another project](https://gitlab.com/Practical.Apps/exam2sql)

If, after reading all this, you would like to support my work, use these to donate:

BTC: 3F2rB1sfxVRCT8HTYXZh7nriZcD6J137Pp\
LTC: MWigH9XSBdig89QLYNsuKmsNSx59RKuX82\
ETH: 0x491DfB927cdeC2d36bf65A9dc57bA7fDC803FC87
